
Based on: https://github.com/JorgeDuenasLerin/diccionario-espanol-txt


```sh
egrep -e "^.....$" 0_palabras_todas.txt |  > 1_universe.txt
cat length/05.txt >> 1_universe.txt
egrep --invert-match '[^a-zA-ZáéíóúÁÉÍÓÚüÜñÑ]' 1_universe.txt > 2_clean_universe.txt
cat 2_clean_universe.txt | sed -e 's/\(.*\)/\L\1/' -e 's~á~a~g' -e 's~é~e~g' -e 's~í~i~g' -e 's~ó~o~g' -e 's~ú~u~g' -e 's~ü~u~g' > 3_universe_lower.txt
cat 3_universe_lower.txt | sort | uniq > 4_universe_wordle.txt
```


```sh
sed 's/./&\n/g' 4_universe_wordle.txt | sort | uniq -ic | sort -V
```


```
   1134 b
   1401 m
   1446 p
   1491 d
   1861 t
   2073 c
   2094 u
   2422 l
   2427 n
   2459 s
   3120 i
   3414 r
   4563 o
   4956 e
   7913 a
   9566 
    105 x
    313 y
    328 ñ
    527 z
    639 v
    673 h
    680 j
    684 f
    960 g
     42 k
     99 q
      6 w
```

```sh
# REQUIRED: install gawk
sudo apt-get --no-install-recommends install gawk
```

```sh
cat 4_universe_wordle.txt | awk '{split($1,a,"");n=asort(a);for(i=1;i<=n;i++){if(a[i]==a[i+1]){next}}}n' > 5_unique_char_words.txt
```


```sh
sed 's/./&\n/g' 5_unique_char_words.txt | sort | uniq -ic | sort -V
```


```
   1127 t
   1223 c
   1330 l
   1430 s
   1535 n
   1555 u
   1964 r
   2087 i
   2722 o
   2811 e
   3621 a
   5565 
    191 y
    218 ñ
    307 z
    360 v
    405 j
    433 f
    444 h
    637 g
    662 b
    849 m
    859 p
    879 d
     26 k
     68 x
     79 q
      3 w
```


