NB_USER=$(shell id -un)
NB_UID=$(shell id -u)
NB_GID=$(shell id -g)
PWD=$(shell pwd)

# jupyter:
# 	docker run \
# 		-it \
# 		--rm \
# 		--name jupyter \
# 		--net host \
# 		--user root \
# 		-e NB_USER="${NB_USER}" \
# 		-e NB_UID="${NB_UID}" \
# 		-e NB_GID="${NB_GID}"  \
# 		-e CHOWN_HOME=yes \
# 		-e CHOWN_HOME_OPTS="-R" \
# 		-w "/home/${NB_USER}" \
# 		-v "${PWD}":/home/"${NB_USER}" \
# 		jupyter/scipy-notebook


hello:
	docker run \
		-it \
		--rm \
		--net host \
		--name python \
		-v "${PWD}":/usr/src/myapp \
		-w /usr/src/myapp \
		python:3-alpine \
		sh -c ' \
			echo "pip install lxml Inflector" && \
			python src/sandbox/hello.py my_arg \
		'

