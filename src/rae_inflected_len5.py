
import os
import logging
import re

from urllib.parse   import quote
from urllib.request import Request, urlopen
from lxml import etree
import time
import argparse

from inflector import Inflector, Spanish

logging.basicConfig(
    format="{asctime} {levelname:.1} {pathname}[{lineno}]: {message}",
    style="{",
    level=logging.DEBUG,
    datefmt="%Y-%m-%d %H:%M:%S"
)
logger = logging.getLogger()

# url_list="https://dle.rae.es/{}?m=31"
url_def="https://dle.rae.es/{}"

# # input="1_universe.txt"
# input=os.environ.get('HOME') + "/diccionario-espanol-txt/0_palabras_todas.txt"
# output=os.environ.get('HOME') + "/2_universe_plural.txt"
input="../diccionario-espanol-txt/0_palabras_todas.txt"
output="./2_universe_plural.txt"

count_request=0

WORD_LEN=5
MAX_ATTEMPS=5

UA="Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"


def get_url(url_template, param):
    return url_template.format(quote(param))

rae_exist_cache={}

def rae_exist(a_string):

    # logger.debug("a_string=%s", a_string)

    if (a_string in rae_exist_cache):
        return rae_exist_cache[a_string]

    url = get_url(url_def, a_string)

    # logger.debug("url=%s ...", url)

    attemps = MAX_ATTEMPS
    webpage = None
    while attemps > 0:
        logger.debug("try=%s url=%s", MAX_ATTEMPS-attemps+1, url)
        # logger.debug("url=%s ...", url)
        try:
            req = Request(url, headers={'User-Agent': UA})
            webpage = urlopen(req)
            tree = etree.parse(webpage, etree.HTMLParser())
            break
        except Exception as e:
            attemps -= 1
            logger.error("str(e)=%s", str(e))
            time.sleep(0.5)

    resp = tree.xpath('//*[@id="resultados"]/*/text()')

    exist = " no está en el Diccionario." not in resp

    rae_exist_cache[a_string]=exist

    logger.debug("try=%s url=%s (%s)", MAX_ATTEMPS-attemps+1, url, exist)

    return exist


def main():

    logger.info("BEGIN")

    inflector = Inflector(Spanish)

    parser = argparse.ArgumentParser(description='Generate length-5 RAE words plus plurals')
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('input', metavar='INPUT_FILE', type=str, nargs='+',
                    help='the input')

    args = parser.parse_args()
    # print(args.accumulate(args.integers))

    # input = args.accumulate(args.input)
    input = args.input[0]
    output = input + '.plus_plurals_len5.txt'

    input_file = open(input, "r")
    output_file = open(output, "w",1)

    logger.debug("input=%s", input)
    logger.debug("output=%s", output)

    for word_sgl in input_file:

        word_sgl = word_sgl.strip()

        logger.debug("----- cached=%5s len=%3s word_sgl=%s", word_sgl in rae_exist_cache, len(word_sgl), word_sgl)

        # Write original word

        have_to_add = len(word_sgl) == WORD_LEN and word_sgl not in rae_exist_cache
        # have_to_add = word_sgl not in rae_exist_cache

        if have_to_add:

            rae_exist_cache[word_sgl] = True

            logger.debug("output_file.write(word_sgl=%s)", word_sgl)
            output_file.write(word_sgl + '\n')
            output_file.flush()

        # Try plural
        word_plu=inflector.pluralize(word_sgl).strip()
        logger.debug("----- cached=%5s len=%3s word_plu=%s", word_plu in rae_exist_cache, len(word_plu), word_plu)

        if len(word_plu) != WORD_LEN :
            # word_pl doesn't have the len
            logger.debug("word_pl doesn't have the len: CONTINUE!",)
            continue

        if word_plu in rae_exist_cache:
            # I already have it
            logger.debug("I already have it: CONTINUE!",)
            continue

        # RAE validation
        exist = rae_exist(word_plu)

        rae_exist_cache[word_plu] = exist

        if not exist:
            # word_pl doesn't exist
            logger.debug("word_pl doesn't exist: CONTINUE!",)
            continue

        logger.debug("output_file.write(word_plu=%s)", word_plu)
        output_file.write(word_plu + '\n')
        output_file.flush()

    input_file.close()
    output_file.close()

    logger.info("END")


if __name__ == "__main__":
    main()


