#!/usr/bin/env python3

# Desarrollado por Jorge Dueñas Lerín

from urllib.parse   import quote
from urllib.request import Request, urlopen
from lxml import etree
import time

import logging
logging.basicConfig(
    # format="{asctime} {levelname:.1}: {message}",
    format="{asctime} {levelname:.1} {pathname}[{lineno}]: {message}",
    style="{",
    level=logging.DEBUG,
    datefmt="%Y-%m-%d %H:%M:%S"
)
logger = logging.getLogger()

logger.info("BEGIN")



RES="res"
CONT_CONJ="cont_conj"
CONJ="conj"

UA="Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"

url_list="https://dle.rae.es/{}?m=31"
url_detail="https://dle.rae.es/{}"

to_remove_from_title='Ir a la entrada '


"""
Usamos title por que el contenido en determinadas situaciones cambia:
<a data-cat="FETCH" data-acc="LISTA EMPIEZA POR" data-eti="abollado" title="Ir a la entrada abollado, abollada" href="/abollado">abollado<sup>1</sup>, da</a>
"""
skip = len(to_remove_from_title)

# letras = ['a', 'á', 'b', 'c', 'd', 'e', 'é', 'f', 'g', 'h', 'i', 'í', 'j', 'k', 'l', 'm',
#              'n', 'ñ', 'o', 'ó', 'p', 'q', 'r', 's', 't', 'u', 'ú', 'ü', 'v', 'w', 'x', 'y', 'z']

# test plural: gatos, peces
# letras = ['gato', 'gemir', 'asdfasdfasdf']
letras = ['g', 'k']


#Para comprobar la necesidad de title
#letras = ['abonado']

start_withs = letras.copy()


# file_name="palabras_todas.txt"
file_name="rae_words.txt"

file_rae_words = open(file_name, "w")


def get_url(url_template, param):
    return url_template.format(quote(param))

def get_xtree(url):

    logger.debug("GET_XTREE(%s)...", url)

    tree = None
    attemps = 5
    while attemps > 0 and tree == None:
        logger.debug("attemps=%s", attemps)
        try:
            req = Request(url, headers={'User-Agent': UA})
            logger.debug("req.full_url=%s", req.full_url)
            webpage = urlopen(req)
            htmlparser = etree.HTMLParser()
            tree = etree.parse(webpage, htmlparser)
        except Exception as e:
            attemps -= 1
            logger.error("str(e)=%s", str(e))
            time.sleep(0.5)

    logger.debug("GET_XTREE(%s)...ok", url)

    return tree


def get_rae_cached(url, cache):

    # logger.debug("cache=%s...", cache)

    if(url in cache):
        return cache[url]

    tree = get_xtree(url)

    cache[url]={
        RES: tree.xpath('//*[@id="resultados"]/*/div[@class="n1"]/a/@title'),
        CONT_CONJ: tree.xpath('//*[@id="resultados"]/*/a[@class="e2"]/@title'),
        CONJ: tree.xpath('//div[@id="conjugacion"]//td//text()'),
    }

    # logger.debug("cache[url]=%s...", cache[url])

    return cache[url]


while len(start_withs) != 0:

    url_cache={}
    word_cache=[]

    logger.debug("%sstart_withs=%s...", "-"*3, start_withs)

    palabra_start_with = start_withs.pop(0)

    if(palabra_start_with in ['app', 'docs', 'js']):
        continue

    my_url = get_url(url_list, palabra_start_with)

    url_cache[my_url] = get_rae_cached(my_url,url_cache)

    res = url_cache[my_url][RES]


    # Se repiten palabras. Cuando por ejemplo aba tiene más de 30 y se exapande
    # abaa, abab, etc... las primeras palabras no aparecen: aba
    # logger.debug("res=%s", res)
    logger.debug("%sres=%s...", "-"*3, res)
    for pal in res:
        logger.debug("%spal=%s...", "-"*6, pal)
        pal_clean = pal[skip:]
        pal_clean = pal_clean.split(", ")
        logger.debug("%spal_clean=%s", "-"*6, pal_clean)
        for p in pal_clean:
            logger.debug("%sp=%s...","-"*9, p)

            if (p not in word_cache):
                logger.debug("%sfile_rae_words.write(%s)!!!","-"*9,p)
                file_rae_words.write(p+'\n')
                # word_cache += p
                word_cache.append(p)

            # Try plural

            # Try conjugación
            logger.debug("%sTry conjugación!!!","-"*9)

            my_url_conj = get_url(url_detail, p)


            url_cache[my_url] = get_rae_cached(my_url_conj,url_cache)
            # contains_conjug = get_rae_cached(my_url_conj,url_cache)[CONJ]

            contains_conjug = url_cache[my_url][CONT_CONJ]

            # logger.debug("%smy_url in url_cache????","-"*9)

            # if(my_url_conj in url_cache):
            #     tree = url_cache[my_url_conj]
            # else:
            #     tree = get_xtree(my_url_conj)
            #     url_cache[my_url_conj]=tree

            # tree = get_xtree(my_url_conj)
            # contains_conjug = tree.xpath('//*[@id="resultados"]/*/a[@class="e2"]/@title')

            # url_cache += my_url_conj

            logger.debug("%scontains_conjug=%s!!!","-"*9,contains_conjug)

            if len(contains_conjug) > 0:
                logger.debug("^" * 80)
                # get all contant in tds
                conjug = url_cache[my_url][CONJ]

                logger.debug("conjug=%s", conjug)

                conjug_clean = ' '.join(conjug).replace(', ', ' ').replace(' / ', ' ').split(' ')
                for conj in conjug_clean:

                    logger.debug("%sconj=%s...","-"*12, conj)

                    if(conj!=''):
                        # logger.debug("conj=%s", conj)
                        if (conj not in word_cache):
                            logger.debug("%sfile_rae_words.write(%s)!!!","-"*12,conj)
                            file_rae_words.write(conj+'\n')
                            # word_cache += conj
                            word_cache.append(conj)


                    logger.debug("%sconj=%s...ok","-"*12, conj)

            logger.debug("%sp=%s...ok","-"*9, p)

        logger.debug("%spal=%s...ok", "-"*6, pal)

    if(len(res)>30):
        logger.debug("!" * 80)
        logger.debug("EXAPEND palabra_start_with=%s", palabra_start_with)
        expand = [palabra_start_with + l for l in letras]
        logger.debug("expand=%s", expand)
        start_withs = expand + start_withs

    logger.debug("%sstart_withs=%s...ok", "-"*3, start_withs)

file_rae_words.close()


logger.info("END")


# exit()
